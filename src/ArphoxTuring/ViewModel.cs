﻿using System;
using System.Windows;
using TuringProcessor;

namespace ArphoxTuring
{
    public class ViewModel : Bindable
    {
        public ViewModel()
        {
            Program = new string[2, 2]
            {
                {"-", "q1"},
                {"|", "!" }
            };
            InputWord = "||";
            StartingPos = 1;
            StartingState = "q1";
        }

        Executor _executor;
        public bool SomethingChanged { get; set; } //Combobox selection, or starting position, or input line

        private string filePath;
        public string FilePath
        {
            get { return filePath; }
            set { filePath = value; RaisePropertyChanged("FilePathLoaded"); }
        }

        string presenterText;
        public string PresenterText
        {
            get { return presenterText; }
            set { presenterText = value;  RaisePropertyChanged(); }
        }

        string inputWord;
        public string InputWord
        {
            get { return inputWord; }
            set { inputWord = value; RaisePropertyChanged(); }
        }

        public int StartingPos { get; set; }
        public string StartingState { get; set; }

        string[,] program;
        public string[,] Program
        {
            get { return program; }
            set
            {
                program = value;
                RaisePropertyChanged();
                StatesRefresh();
                SomethingChanged = true;
            }
        }

        public TuringProgram TuringProgram { get; set; }
        public TuringSolver TuringSolver { get; set; }

        public string[] States { get; set; }
        public void StatesRefresh()
        {
            if (Program == null || Program.GetLength(1) < 2)
                States = null;

            States = new string[Program.GetLength(1)];

            for (int i = 1; i < States.Length; i++)
                States[i] = Program[0, i];

            RaisePropertyChanged("States");
            SomethingChanged = true; //So if the Program gets modified, it gets to true
        }

        public void UpdatePresenter()
        {
            PresenterText = Helper.GetReaderString(TuringSolver.ReaderPosition, TuringSolver.InputLine.Length, TuringSolver.ReaderState);
        }

        public void Solve()
        {
            TuringProgram = new TuringProgram(program);
            TuringSolver = new TuringSolver(TuringProgram, InputWord, StartingPos, StartingState);

            _executor = new Executor(TuringProgram, TuringSolver);
            try
            {
                _executor.Solve();
                InputWord = TuringSolver.InputLine;
                UpdatePresenter();
            }
            catch (TimeoutException ex)
            {
                MessageBox.Show(ex.Message, "Hiba", MessageBoxButton.OK, MessageBoxImage.Error);
            }
        }
        public void PerformStep()
        {
            if (SomethingChanged)
            {
                TuringProgram = new TuringProgram(program);
                TuringSolver = new TuringSolver(TuringProgram, InputWord, StartingPos, StartingState);
                _executor = new Executor(TuringProgram, TuringSolver);
                SomethingChanged = false;
            }
            try
            {
                _executor.PerformStep();
                InputWord = TuringSolver.InputLine;
                UpdatePresenter();
            }
            catch(ApplicationException apex)
            {   //Already Solved, but np:
                InputWord = TuringSolver.InputLine;
                MessageBox.Show(apex.Message, "Üzenet", MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
        }

        public void SaveFile(string path)
        {
            TuringProcessor.FileOperator.WriteFile(Program, path, '\t');
        }
    }
    class Helper
    {
        public static string GetReaderString(int readerPosition, int wordLength, string readerState)
        {
            string a = string.Empty;

            //Inserting reader position char 
            for (int i = 0; i < readerPosition; i++)
                a += " ";

            a += Symbols.ReaderPosSymbol;

            for (int i = readerPosition; i < wordLength; i++)
                a += " ";

            a += "\n";

            //Inserting state char
            for (int i = 0; i < readerPosition; i++)
                a += " ";
            a += readerState;
            for (int i = readerPosition; i < wordLength; i++)
                a += " ";

            return a;
        }
    }
}