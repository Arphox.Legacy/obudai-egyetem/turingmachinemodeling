﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace ArphoxTuring
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ViewModel vm { get; private set; }
        public DispatcherTimer afterCellEditEndingTimer = new DispatcherTimer();
        public MainWindow()
        {
            InitializeComponent();
            vm = new ViewModel();
            this.DataContext = vm;
            Loaded += (sender, e) => MoveFocus(new TraversalRequest(FocusNavigationDirection.Next)); //On start, Focus the first TextBox

            afterCellEditEndingTimer.Interval = new TimeSpan(0, 0, 0, 0, 10);
            afterCellEditEndingTimer.Tick += afterCellEditEndingTimer_Tick;
        }

        private void afterCellEditEndingTimer_Tick(object sender, EventArgs e)
        {
            vm.StatesRefresh();
            afterCellEditEndingTimer.Stop();
        }

        private void dataGrid2D_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            if (e.EditAction == DataGridEditAction.Commit) //If commiting the change (not cancelling)
            {
                string newCellValue = (e.EditingElement as TextBox).Text;

                if (string.IsNullOrEmpty(newCellValue))
                    (e.EditingElement as TextBox).Text = "-";

                //The edit will be seen in the 2D array AFTER this method completed, so
                //I have to wait for a little bit, and then refresh the states. A bit of silly solution, but works
                afterCellEditEndingTimer.Start();
            }
        }


        private void ButtonLoad_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog ablak = new OpenFileDialog();
            ablak.InitialDirectory = Environment.CurrentDirectory;
            ablak.Filter = "Szöveges fájlok (*.txt)|*.txt";

            if (ablak.ShowDialog() == true)
            {
                vm.FilePath = ablak.FileName;

                vm.TuringProgram = new TuringProcessor.TuringProgram(vm.FilePath, false);
                vm.Program = vm.TuringProgram.GetSimplifiedProgram;
            }
        }
        private void ButtonSave_Click(object sender, RoutedEventArgs e)
        {
            SaveFileDialog sfd = new SaveFileDialog();
            sfd.Title = "Program mentése";
            sfd.Filter = "Szöveges fájl | *.txt";
            sfd.DefaultExt = "txt";
            sfd.ValidateNames = true;

            if (sfd.ShowDialog() == true)
            {
                vm.SaveFile(sfd.FileName);
            }
        }
        private void ButtonGuide_Click(object sender, RoutedEventArgs rea)
        {
            string formatted =
            "1. Készítsd el a Turing-programod az alsó szerkesztő használatával!\n" +
            "Oszlopot és sort hozzáadni/törölni az oldalon és lent található [ + ] és [ - ] gombokkal tudsz.\n" +
            "A 0. sor és 0. oszlop speciális, ott kell felsorolni a jeleket és az állapotokat. Példákért lásd a minta txt fáljokat a könyvtárban." +
            "Az üres cellákat kötőjellel ('-') jelöld.\n" +

            "\n2. Add meg az input szót, a kezdő pozíciót és a kezdő állapotot.\n" +

            "\n3. Ha minden adatot megadtál, akkor lefuttathatod a programodat:\n" +
            "[ Lép ] gomb: elvégez egy lépést és jelzi a jelenlegi pozíciót és állapotot\n" +
            "[ Megold ] gomb: addig futtatja a programot, amíg \"nincs vége\", vagy az előzetesen beállított időtartamig (ez általában néhány másodperc).\n" +

            "\nNéhány funkció rövid leírását a Tooltipjeként is láthatod (ha az egérmutatót az elem (pl. gomb, mező) fölé mozgatod és vársz egy kicsit)" +

            "\n\nSok sikert kíván az FNyG tárgyhoz a fejlesztő: Ozsvárt Károly\n/2016/\n" +
            "Jelenlegi verzió: " + Settings.VERSION;

            MessageBox.Show(formatted, "Útmutató", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void ButtonNewColumn_Click(object sender, RoutedEventArgs e)
        {
            vm.Program = ArphoxUtil.ArrayOperations.AddNewColumn(vm.Program, "-");
        }
        private void ButtonRemoveColumn_Click(object sender, RoutedEventArgs e)
        {
            if (vm.Program.GetLength(1) > 2)
                vm.Program = ArphoxUtil.ArrayOperations.RemoveColumn(vm.Program, vm.Program.GetLength(1) - 1);
        }

        private void ButtonNewRow_Click(object sender, RoutedEventArgs e)
        {
            vm.Program = ArphoxUtil.ArrayOperations.AddNewRow(vm.Program, "-");
        }
        private void ButtonRemoveRow_Click(object sender, RoutedEventArgs e)
        {
            if (vm.Program.GetLength(0) > 2)
                vm.Program = ArphoxUtil.ArrayOperations.RemoveRow(vm.Program, vm.Program.GetLength(0) - 1);
        }

        private void ButtonStep_Click(object sender, RoutedEventArgs rea)
        {
            try
            {
                vm.PerformStep();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        private void ButtonSolve_Click(object sender, RoutedEventArgs rea)
        {
            try
            {
                vm.Solve();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }


        private void UISettingChanged(object sender, EventArgs e)
        {
            //If user changes the InputWord OR StartingPosition OR Starting
            vm.SomethingChanged = true;
        }
        private void TextBoxInputWord_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            UISettingChanged(sender, e);

            #region GreekKeyChanger
            //string newValue = HelperClass.GreekKeyDownHandler(e);

            //if (newValue != null)
            //{
            //    e.Handled = true; //handle the event and cancel the original key
            //    int tbPos = TextBoxInputWord.SelectionStart; //get caret position
            //    TextBoxInputWord.Text = TextBoxInputWord.Text.Insert(tbPos, newValue); //insert the new text at the caret position

            //    //replace the caret back to where it should be 
            //    //otherwise the insertion call above will reset the position
            //    TextBoxInputWord.Select(tbPos + 1, 0);
            //}
            #endregion
        }
    }
    public static class HelperClass
    {
        public static string GreekKeyDownHandler(KeyEventArgs e)
        {
            string newValue = null;

            if (Keyboard.Modifiers != ModifierKeys.Control)
                return newValue;

            if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.A) newValue = "α";       //alpha
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.B) newValue = "β";  //beta
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.C) newValue = "ξ";  //xi
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.D) newValue = "δ";  //delta
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.E) newValue = "ε";  //epsilon
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.F) newValue = "φ";  //phi
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.G) newValue = "γ";  //gamma
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.H) newValue = "η";  //eta
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.I) newValue = "ι";  //iota
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.J) newValue = "∃"; //NOT EXISTS
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.K) newValue = "κ";  //kappa
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.L) newValue = "λ";  //lambda
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.M) newValue = "μ";  //mu
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.N) newValue = "ν";  //nu
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.O) newValue = "ο";  //omicron
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.P) newValue = "π";  //pi
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.Q) newValue = "θ";  //theta
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.R) newValue = "ρ";  //rho
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.S) newValue = "σ";  //sigma
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.T) newValue = "τ";  //tau
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.U) newValue = "υ";  //upsilon
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.V) newValue = "∀"; //EVERY
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.W) newValue = "ω";  //omega
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.X) newValue = "χ";  //chi
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.Y) newValue = "ψ";  //psi
            else if (Keyboard.Modifiers == ModifierKeys.Control && e.Key == Key.Z) newValue = "ζ";  //zeta

            return newValue;
        }

    }
}