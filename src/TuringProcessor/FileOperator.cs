﻿using System.Text;
using System.IO;
using System.Collections.Generic;
using System.Linq;

namespace TuringProcessor
{
    //Code-check ✔
    public static class FileOperator
    {
        /// <summary>
        /// Reads the file.
        /// </summary>
        /// <param name="path">Path for the file to read.</param>
        /// <param name="separatorChar">Defines the character that separates the items from each other.</param>
        /// <returns></returns>
        internal static string[,] ReadFile(string path, char separatorChar = '\t')
        {
            string[] fileRows = File.ReadAllLines(path, Encoding.UTF8);

            fileRows = RemoveInvalidRows(fileRows); //filter commented and empty lines

            int numOfCols = fileRows[0].Split(separatorChar).Length;

            string[,] output = new string[fileRows.Length, numOfCols];

            for (int i = 0; i < fileRows.Length; i++) //iterate through rows
            {
                string[] row = fileRows[i].Split(separatorChar); //get splitted current row

                for (int j = 0; j < numOfCols; j++) //iterate through the actual row's columns
                    output[i, j] = row[j];
            }

            return output;
        }

        /// <summary>
        /// Removes all invalid rows, including commented (//....), empty, null and whitespace entries.
        /// </summary>
        /// <param name="fileRows">The file's contents. Every row has to be one string in the array.</param>
        /// <returns></returns>
        internal static string[] RemoveInvalidRows(string[] fileRows)
        {
            List<string> newRows = new List<string>();

            for (int i = 0; i < fileRows.Length; i++)
            {
                if (! //If the following conditions are not true:
                    (
                    fileRows[i].StartsWith("//") ||             //commented line OR
                    string.IsNullOrEmpty(fileRows[i]) ||        //null or empty line OR
                    string.IsNullOrWhiteSpace(fileRows[i]))     //null or whitespace line
                    )
                {
                    newRows.Add(fileRows[i]);                   //Add to the new list
                }
            }
            return newRows.ToArray();
        }

        /// <summary>
        /// Writes the program to the file specified in the path (UTF-8 Encoding).
        /// If the file already exists, it overwrites.
        /// </summary>
        /// <param name="program"></param>
        /// <param name="path"></param>
        /// <param name="separatorChar"></param>
        public static void WriteFile(string[,] program, string path, char separatorChar = '\t')
        {
            using (StreamWriter sw = new StreamWriter(path, false, Encoding.UTF8))
            {
                int i = 0, j = 0;
                for (i = 0; i < program.GetLength(0); i++)
                {
                    for (j = 0; j < program.GetLength(1) - 1; j++) //For every column except the last, write with separator char at the end
                        sw.Write(program[i, j] + separatorChar);

                    if (i == program.GetLength(0) - 1 && j == program.GetLength(1) - 1) //For last element, just the element
                        sw.Write(program[i, j]);
                    else
                        sw.Write(program[i, j] + sw.NewLine);       //In every other cases, write the element and a new line after it
                }
            }
        }
    }
}